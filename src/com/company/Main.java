package com.company;

public class Main {

    public static void main(String[] args) {
        Stack stack = new Stack();
        System.out.println("get: " + stack.get());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        System.out.println("get: " + stack.get());
        System.out.println("pop: " + stack.pop());
        System.out.println("get: " + stack.get());
    }
}
